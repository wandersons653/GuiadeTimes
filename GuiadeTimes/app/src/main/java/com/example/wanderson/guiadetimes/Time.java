package com.example.wanderson.guiadetimes;

public class Time {

    private Integer id;
    private String nome;
    private String serie;
    private int gols;

    public String toString() {
        return "Time: "+nome+"      Série: "+serie+"      Gols: "+gols;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public int getGols() {
        return gols;
    }

    public void setGols(int gols) {
        this.gols = gols;
    }
}
