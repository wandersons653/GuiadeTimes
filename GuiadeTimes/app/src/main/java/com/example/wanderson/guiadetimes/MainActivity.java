package com.example.wanderson.guiadetimes;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    boolean status = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.AtualizaListView ();

    }
    public void AtualizaListView(){

        ListView lista =(ListView)findViewById(R.id.lista);
        ArrayAdapter<Time> addTime = new ArrayAdapter<Time>(this,android.R.layout.simple_list_item_1,getListarTime());
        lista.setAdapter(addTime);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView adapter, View view, int i, long l) {

                Log.d("DB", "Iniciando");
                Time t = (Time)adapter.getItemAtPosition(i);

                Intent intent = new Intent(getApplicationContext(),Cadastro.class);

                intent.putExtra("status", status);
                intent.putExtra("id",t.getId());
                intent.putExtra("nome",t.getNome());
                intent.putExtra("serie",t.getSerie());
                intent.putExtra("gols",t.getGols());

                startActivity(intent);
            }
        });
    }

    public List<Time> getListarTime() {

        Log.d("DB", "Conectando");
        Banco banco = new Banco(getBaseContext());
        SQLiteDatabase conexao = banco.getReadableDatabase();

        Cursor c = conexao.query(
                "time", new String[]{"id", "nome", "serie", "gols"},
                null, null, null, null, null, null
        );

        Log.d("DB", "Criando lista");
        List<Time> lista2 = new ArrayList<Time>();

        while (c.moveToNext()) {

            Time time = new Time();
            time.setId(c.getInt(0));
            time.setNome(c.getString(1));
            time.setSerie(c.getString(2));
            time.setGols(c.getInt(3));
            lista2.add(time);

        }

        return lista2;
    }
    public void proximaTela(View view){
        Intent intent = new Intent(getApplicationContext(),Cadastro.class);
        intent.putExtra("status", !status);
        startActivity(intent);
    }
}
