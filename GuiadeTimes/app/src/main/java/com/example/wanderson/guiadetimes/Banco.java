package com.example.wanderson.guiadetimes;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class Banco extends SQLiteOpenHelper {

    public Banco(Context c) {
        super(c, "Times", null, 2);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL("create table time ( id integer primary key, nome text unique, serie text, gols integer)");
               Log.d("DB", "Criado");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void Insert(Time time, Context c){

        SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put("nome", time.getNome());
            values.put("serie", time.getSerie());
            values.put("gols", time.getGols());

        db.beginTransaction();
        try {

            long result = db.insert("time", null, values);
            db.setTransactionSuccessful();

            if (result > 0){
                Toast.makeText(c, "Salvo com Sucesso!", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(c, MainActivity.class);
                c.startActivity(intent);
            }else{
                Toast.makeText(c, "Erro, Tente novamente! ", Toast.LENGTH_LONG).show();
            }

        } finally {
            db.endTransaction();
        }

    }
    public void Update(Time time, Context c){

        SQLiteDatabase db = this.getWritableDatabase ();


        ContentValues values = new ContentValues();
        String where = "id="+time.getId();
        values.put("id", time.getId());
        values.put("nome", time.getNome());
        values.put("serie", time.getSerie());
        values.put("gols", time.getGols());

        db.beginTransaction();
        try {

            long result = db.update("time", values, where, null);
            db.setTransactionSuccessful();

            if (result > 0){
                Toast.makeText(c, "Alterado com sucesso!", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(c,MainActivity.class);
                c.startActivity(intent);
            }else {
                Toast.makeText (c, "Erro, Tente novamente! ", Toast.LENGTH_LONG).show ();
            }
        }finally {
            db.endTransaction();
        }
    }

    public void Delete(Time time, Context c){

        SQLiteDatabase db = this.getWritableDatabase ();


        ContentValues values = new ContentValues();

        String table = "time";
        String where = "id="+time.getId();

        db.beginTransaction();
        try {

            long result = db.delete (table, where, null);
            db.setTransactionSuccessful();

            if (result > 0){
                Toast.makeText(c, "Time excluido!", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(c,MainActivity.class);
                c.startActivity(intent);
            }

        }catch(Exception e){
            Log.i("Error", "Erro ao atualizar dados! Códido: "+e);
        } finally {
            db.endTransaction();
        }
    }
}
