package com.example.wanderson.guiadetimes;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static java.lang.Integer.parseInt;

public class Cadastro extends AppCompatActivity {

    Integer id;
    EditText nome;
    EditText serie;
    EditText gols;
    Button excluir;
    Button salvar;
    Button editar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        id = null;
        nome = (EditText) findViewById(R.id.nome);
        serie = (EditText) findViewById(R.id.serie);
        gols = (EditText) findViewById(R.id.gols);
        excluir = (Button) findViewById(R.id.excluir);
        salvar = (Button) findViewById(R.id.salvar);
        editar = (Button) findViewById(R.id.editar);


        if (getIntent().getExtras().getBoolean("status") == true){
            Integer id2 = getIntent().getExtras().getInt("id");
            String nome2 = getIntent().getExtras().getString("nome");
            String serie2 = getIntent().getExtras().getString("serie");
            int gols2 = getIntent().getExtras().getInt("gols");


            id = id2;
            nome.setText(nome2);
            serie.setText(serie2);
            gols.setText(Integer.toString(gols2));

            excluir.setVisibility(View.VISIBLE);
            salvar.setText("Atualizar");
            salvar.setVisibility (View.INVISIBLE);
            editar.setVisibility (View.VISIBLE);

            nome.setEnabled (false);
            serie.setEnabled (false);
            gols.setEnabled (false);
        }
    }
    public void Salvar_Atualizar(View view){

        Time t = new Time();
        Banco bd = new Banco(getBaseContext());


        if ( nome.getText().toString().equals ("") || serie.getText().toString().equals ("") || gols.getText().toString ().equals ("")){
            Toast.makeText(this, "Preencha todos os campos!", Toast.LENGTH_LONG).show();
        }else{

            t.setId(id);
            t.setNome(nome.getText().toString());
            t.setSerie(serie.getText().toString());
            t.setGols(parseInt(gols.getText().toString()));

            if (id != null){
                bd.Update(t, getApplicationContext ());
            }else{
                bd.Insert(t, getApplicationContext ());
            }
        }
    }

    public void Excluir(View view){
        Time t = new Time();
        Banco bd = new Banco(getBaseContext());

        t.setId (id);

        bd.Delete (t, getApplicationContext ());
    }
    public void LiberarCampos(View view){

        editar.setVisibility(View.INVISIBLE);
        salvar.setVisibility (View.VISIBLE);

        nome.setEnabled (true);
        serie.setEnabled (true);
        gols.setEnabled (true);
    }
}
